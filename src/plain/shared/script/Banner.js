import politeLoadImages from '@mediamonks/display-temple/util/politeLoadImages';

export default class Banner {

  constructor(config) {
    // add required components here
    this.config = config;
  }

  async init() {
    this.banner = document.body.querySelector('.banner');
    await politeLoadImages(this.banner)

    this.domMainExit = document.body.querySelector('.mainExit');

    this.domMainExit.addEventListener('click', this.handleClick);
    this.domMainExit.addEventListener('mouseover', this.handleRollOver);
    this.domMainExit.addEventListener('mouseout', this.handleRollOut);
  }

  setAnimation(animation){
    this.animation = animation;
  }

  handleExit = () => {
    window.open(window.clickTag, '_blank');
    this.animation.getTimeline().progress(1);
  };

  /**
   * When client clicks this function will be triggerd.
   */
  handleClick = () => {
    this.handleExit();
  };

  /**
   * When mouse rolls over unit.
   */
  handleRollOver = () => {
    gsap.fromTo('#shineElement', {x:0},{duration:.6, x:520, transformOrigin:'left', ease:'power4.inOut'})
  };

  /**
   * When mouse rolls out unit.
   */
  handleRollOut = () => {
  };

  async start() {
    await this.init();

    this.animation.play();
  }
}

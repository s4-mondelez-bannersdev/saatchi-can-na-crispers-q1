import FrameAnimation from "@mediamonks/display-temple/animation/FrameAnimation"

export default class Animation extends FrameAnimation {
  /**
   *
   * @param {HTMLDivElement} container
   * @param {null} config
   */
  constructor(container) {
    super();

    this.container = container;
  }

  /**
   *
   * @param {gsap.core.Timeline} tl
   */
  frame0(tl){
    tl.addLabel("start")
    .to('.content', {duration:.5, opacity: 1}, "start")

    .from('.bold_holder', {duration:.4, x:-2, rotateX:'90deg', transformOrigin:'center', ease:'power1.inOut'}, 'start+=1')
    .from('.copy_bold',{duration:.4, css:{filter:"blur(3px)"}, ease:'power1.out'}, 'start+=1')

    .to('.bold_holder', {duration:.4, x:-2, rotateX:'-=90deg', transformOrigin:'center', ease:'power1.inOut'}, 'start+=2')
    .to('.copy_bold',{duration:.4, css:{filter:"blur(3px)"}, ease:'power1.out'}, 'start+=2')

    .from('.crispers_holder', {duration:.4, x:-2, rotateX:'90deg', transformOrigin:'center', ease:'power1.inOut'}, 'start+=2')
    .from('.copy_crispers',{duration:.4, css:{filter:"blur(3px)"}, ease:'power1.out'}, 'start+=2')

    .from('.cta_holder', {duration:.4, x:-2, rotateX:'90deg', transformOrigin:'center', ease:'power1.inOut'}, 'start+=2.2')

    .to('#shineElement', {duration:.6, x:520, transformOrigin:'left', ease:'power4.inOut'}, 'start+=3')
  }
}
